package com.javafullstack.nextlms.service;

import com.javafullstack.nextlms.mapper.UserMapper;
import com.javafullstack.nextlms.model.UserDao;
import com.javafullstack.nextlms.repository.UserRepo;
import com.javafullstack.resources.models.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private UserRepo userRepo;
    private UserMapper userMapper;

//     NO NEED for this default constructor
//    public UserService() {
//    }

    public UserService(UserRepo userRepo, UserMapper userMapper) {
        this.userRepo = userRepo;
        this.userMapper = userMapper;
    }

    public List<User> findAll() {

        List<UserDao> users = userRepo.findAll();
        return users.stream().map(m -> {
            User user = new User();
            userMapper.mapDtoToUser(m, user);
            return user;
        }).collect(Collectors.toList());

    }

    public User saveUser(User body) {
        UserDao userDao = new UserDao();
        userMapper.mapUserToDto(body, userDao);
        userMapper.mapDtoToUser(userRepo.save(userDao), body);
        return body;
    }

    public void deleteUserById(Long userId) {
        userRepo.deleteById(userId);
    }

    public User getById(Long userId) {
        UserDao userDao = userRepo.findById(userId).get();
        User user = new User();
        userMapper.mapDtoToUser(userDao, user);
        return user;
    }
}
