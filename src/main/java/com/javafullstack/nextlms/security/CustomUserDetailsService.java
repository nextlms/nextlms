package com.javafullstack.nextlms.security;

import com.javafullstack.nextlms.model.UserDao;
import com.javafullstack.nextlms.repository.UserRepo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private UserRepo users;

    public CustomUserDetailsService(UserRepo users) {
        this.users = users;
    }

    @Override
    public UserDao loadUserByUsername(String username) throws UsernameNotFoundException {
        return this.users.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username: " + username + " not found"));
    }

    public static UserDetails create(UserDao user) {
        List<String> authorities = user.getRoles().stream().map(role ->
                new String(role)
        ).collect(Collectors.toList());

        return new UserDao(
                user.getFirstName(),
                user.getUsername(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                authorities
        );
    }
}
