package com.javafullstack.nextlms.controllers;

import com.javafullstack.nextlms.model.JwtAuthenticationResponse;
import com.javafullstack.nextlms.model.LoginRequest;
import com.javafullstack.nextlms.repository.UserRepo;
import com.javafullstack.nextlms.security.JwtTokenProvider;
import com.javafullstack.nextlms.service.UserService;
import com.javafullstack.resources.models.User;
import com.javafullstack.resources.rest.interfaces.UsersApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
//@RequestMapping("/api")
public class UserController implements UsersApi {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepo users;

    private UserService userService;
    private JwtTokenProvider jwtTokenProvider;

    public UserController(UserService userService, JwtTokenProvider jwtTokenProvider) {
        this.userService = userService;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @PostMapping("/oauth/token")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtTokenProvider.createToken(loginRequest.getUsernameOrEmail(),
                this.users.findByUsername(loginRequest.getUsernameOrEmail())
                        .orElseThrow(() -> new UsernameNotFoundException("Username " + loginRequest.getUsernameOrEmail() + "not found")).getRoles());
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
    }

    @Override
    public ResponseEntity<User> addUser(User body) {
        User user = userService.saveUser(body);
        if(user != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(user);
        }
        else{
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    public ResponseEntity<String> deleteUser(Long userId) {
        User user = userService.getById(userId);
        if(user != null) {
            userService.deleteUserById(userId);
            return ResponseEntity.status(HttpStatus.OK).body("{\"userId deleted\":\"" + userId + "\"}");
        }
        else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("{\"userId doesn't exist\"" + userId + "\"}");
        }
    }

    @Override
    public ResponseEntity<List<User>> findAllUsers(){
        List<User> users = userService.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(users);
    }

    @Override
    public ResponseEntity<User> getUserById(Long userId) {
        User user = userService.getById(userId);
        if(user != null){
            return ResponseEntity.status(HttpStatus.OK).body(user);
        }
        return ResponseEntity.badRequest().build();
    }

    @Override
    public ResponseEntity<User> updateUser(User body) {
        User user = userService.saveUser(body);
        if(user != null){
            return ResponseEntity.status(HttpStatus.CREATED).body(user);
        }
        else{
            return ResponseEntity.badRequest().build();
        }
    }
}
