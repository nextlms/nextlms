package com.javafullstack.nextlms.repository;

import com.javafullstack.nextlms.model.UserDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<UserDao, Long> {
    <T> Optional<UserDao> findByUsername(String username);
}
