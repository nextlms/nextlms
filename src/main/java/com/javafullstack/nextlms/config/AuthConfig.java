package com.javafullstack.nextlms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

@Configuration
@EnableAuthorizationServer
public class AuthConfig extends AuthorizationServerConfigurerAdapter {

    private AuthenticationManager authenticationManagerBean;

    @Autowired
    public void setAuthenticationManagerBean(AuthenticationManager authenticationManagerBean) {
        this.authenticationManagerBean = authenticationManagerBean;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory().
                withClient("client").
                secret("{noop}secret123").
                authorizedGrantTypes("password").
                resourceIds("oauth2-resource").
                scopes("read","write","admin");
    }

 // IF USING IN-MEMORY USER DETAILS RATHER THAN GETTING THEM FROM THE DATABASE
/*
    @Bean
    public InMemoryUserDetailsManager userDetailsService() {
        return new InMemoryUserDetailsManager(
                User.builder()
                .username("user")
                .password("user")
                .authorities("ROLE_ADMIN")
                .build()
        );
    }
*/

// COMMENTING THIS BELOW CODE WILL THROW "error_description": "Unsupported grant type: password" EXCEPTION
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                //.userDetailsService(userDetailsService())
                .authenticationManager(authenticationManagerBean);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.passwordEncoder(NoOpPasswordEncoder.getInstance());
        //security.passwordEncoder(new BCryptPasswordEncoder());
        //security.addTokenEndpointAuthenticationFilter(new JwtTokenFilter(new JwtTokenProvider()));
    }

}
