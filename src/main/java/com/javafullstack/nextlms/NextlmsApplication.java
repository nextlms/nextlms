package com.javafullstack.nextlms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableResourceServer
public class NextlmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(NextlmsApplication.class, args);
	}

}
