package com.javafullstack.nextlms.mapper;

import com.javafullstack.nextlms.model.UserDao;
import com.javafullstack.resources.models.User;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    public void mapUserToDto(User src, UserDao dest){
        dest.setId(src.getId());
        dest.setUsername(src.getUsername());
        dest.setFirstName(src.getFirstName());
        dest.setLastName(src.getLastName());
        dest.setEmail(src.getEmail());
        dest.setPhone(src.getPhone());
        dest.setRole( src.getRole().stream().map(s->s.toString()).collect(Collectors.joining(",")));
        dest.setPassword(src.getPassword());
        dest.setRoleId(src.getRoleId());
    }

    public void mapDtoToUser(UserDao src, User dest){
        dest.setId(src.getId());
        dest.setUsername(src.getUsername());
        dest.setFirstName(src.getFirstName());
        dest.setLastName(src.getLastName());
        dest.setEmail(src.getEmail());
        dest.setPhone(src.getPhone());
        dest.setRole(Arrays.asList(src.getRole().split(",")).stream().map(s->User.RoleEnum.valueOf(s)).collect(Collectors.toList()));
        dest.setRoleId(src.getRoleId());
    }
    // return Arrays.toString(strArray);
    // joinedString = String.join(" ", strArray);
    // return joinedString;
    // String[] abc = "xyz".split("");
    // String[] strArray = new String[] {strName};
}
